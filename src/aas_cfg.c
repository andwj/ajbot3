/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

#include <stddef.h>

#include "main.h"

#include "../qcommon/aasfile.h"
#include "aas_cfg.h"
#include "aas_store.h"

// the global AAS config
aas_config_t aascfg;


#define CFG_OFS(x)  (size_t)&(((aas_config_t *)0)->x)

typedef struct cfgfielddef_s
{
	const char *name;	// name of the field
	size_t offset;		// offset in the structure
} cfgfielddef_t;

static cfgfielddef_t cfg_fields[] =
{
	// phys_gravitydirection is handled specially

	{ "phys_friction",				CFG_OFS(phys_friction) },
	{ "phys_stopspeed",				CFG_OFS(phys_stopspeed) },
	{ "phys_gravity",				CFG_OFS(phys_gravity) },
	{ "phys_waterfriction",			CFG_OFS(phys_waterfriction) },
	{ "phys_watergravity",			CFG_OFS(phys_watergravity) },
	{ "phys_maxvelocity",			CFG_OFS(phys_maxvelocity) },
	{ "phys_maxwalkvelocity",		CFG_OFS(phys_maxwalkvelocity) },
	{ "phys_maxcrouchvelocity",		CFG_OFS(phys_maxcrouchvelocity) },
	{ "phys_maxswimvelocity",		CFG_OFS(phys_maxswimvelocity) },

	{ "phys_strafejumping",			CFG_OFS(phys_strafejumping) },
	{ "phys_walkaccelerate",		CFG_OFS(phys_walkaccelerate) },
	{ "phys_airaccelerate",			CFG_OFS(phys_airaccelerate) },
	{ "phys_swimaccelerate",		CFG_OFS(phys_swimaccelerate) },
	{ "phys_maxstep",				CFG_OFS(phys_maxstep) },
	{ "phys_maxsteepness",			CFG_OFS(phys_maxsteepness) },
	{ "phys_maxwaterjump",			CFG_OFS(phys_maxwaterjump) },
	{ "phys_maxbarrier",			CFG_OFS(phys_maxbarrier) },
	{ "phys_jumpvel",				CFG_OFS(phys_jumpvel) },
	{ "phys_falldelta5",			CFG_OFS(phys_falldelta5) },
	{ "phys_falldelta10",			CFG_OFS(phys_falldelta10) },

	{ "rs_waterjump",				CFG_OFS(rs_waterjump) },
	{ "rs_teleport",				CFG_OFS(rs_teleport) },
	{ "rs_barrierjump",				CFG_OFS(rs_barrierjump) },
	{ "rs_startcrouch",				CFG_OFS(rs_startcrouch) },
	{ "rs_startgrapple",			CFG_OFS(rs_startgrapple) },
	{ "rs_startwalkoffledge",		CFG_OFS(rs_startwalkoffledge) },
	{ "rs_startjump",				CFG_OFS(rs_startjump) },
	{ "rs_rocketjump",				CFG_OFS(rs_rocketjump) },
	{ "rs_bfgjump",					CFG_OFS(rs_bfgjump) },
	{ "rs_jumppad",					CFG_OFS(rs_jumppad) },
	{ "rs_aircontrolledjumppad",	CFG_OFS(rs_aircontrolledjumppad) },
	{ "rs_funcbob",					CFG_OFS(rs_funcbob) },
	{ "rs_startelevator",			CFG_OFS(rs_startelevator) },
	{ "rs_falldamage5",				CFG_OFS(rs_falldamage5) },
	{ "rs_falldamage10",			CFG_OFS(rs_falldamage10) },
	{ "rs_maxfallheight",			CFG_OFS(rs_maxfallheight) },
	{ "rs_maxjumpfallheight",		CFG_OFS(rs_maxjumpfallheight) },

	{NULL, 0}
};

// the default Q3A configuration
void DefaultCfg(void)
{
	aascfg.allpresencetypes = PRESENCE_NORMAL|PRESENCE_CROUCH;
	aascfg.numbboxes = 2;

	//bbox 0
	aascfg.bboxes[0].presencetype = PRESENCE_NORMAL;
	aascfg.bboxes[0].flags = 0;
	aascfg.bboxes[0].mins[0] = -15;
	aascfg.bboxes[0].mins[1] = -15;
	aascfg.bboxes[0].mins[2] = -24;
	aascfg.bboxes[0].maxs[0] = 15;
	aascfg.bboxes[0].maxs[1] = 15;
	aascfg.bboxes[0].maxs[2] = 32;

	//bbox 1
	aascfg.bboxes[1].presencetype = PRESENCE_CROUCH;
	aascfg.bboxes[1].flags = 1;
	aascfg.bboxes[1].mins[0] = -15;
	aascfg.bboxes[1].mins[1] = -15;
	aascfg.bboxes[1].mins[2] = -24;
	aascfg.bboxes[1].maxs[0] = 15;
	aascfg.bboxes[1].maxs[1] = 15;
	aascfg.bboxes[1].maxs[2] = 16;

	aascfg.phys_gravitydirection[0]	= 0;
	aascfg.phys_gravitydirection[1]	= 0;
	aascfg.phys_gravitydirection[2]	= -1;

	aascfg.phys_friction			= 6;
	aascfg.phys_stopspeed			= 100;
	aascfg.phys_gravity				= 800;
	aascfg.phys_waterfriction		= 1;
	aascfg.phys_watergravity		= 400;
	aascfg.phys_maxvelocity			= 320;
	aascfg.phys_maxwalkvelocity		= 320;
	aascfg.phys_maxcrouchvelocity	= 100;
	aascfg.phys_maxswimvelocity		= 150;
	aascfg.phys_walkaccelerate		= 10;
	aascfg.phys_airaccelerate		= 1;
	aascfg.phys_swimaccelerate		= 4;
	aascfg.phys_maxstep				= 19;
	aascfg.phys_maxsteepness		= 0.7;
	aascfg.phys_maxwaterjump		= 18;
	aascfg.phys_maxbarrier			= 33;
	aascfg.phys_jumpvel				= 270;
	aascfg.phys_falldelta5			= 40;
	aascfg.phys_falldelta10			= 60;
	aascfg.phys_strafejumping		= 1;

	aascfg.rs_waterjump				= 400;
	aascfg.rs_teleport				= 50;
	aascfg.rs_barrierjump			= 100;
	aascfg.rs_startcrouch			= 300;
	aascfg.rs_startgrapple			= 500;
	aascfg.rs_startwalkoffledge		= 70;
	aascfg.rs_startjump				= 300;
	aascfg.rs_rocketjump			= 500;
	aascfg.rs_bfgjump				= 500;
	aascfg.rs_jumppad				= 250;
	aascfg.rs_aircontrolledjumppad	= 300;
	aascfg.rs_funcbob				= 300;
	aascfg.rs_startelevator			= 50;
	aascfg.rs_falldamage5			= 300;
	aascfg.rs_falldamage10			= 500;
	aascfg.rs_maxfallheight			= 0;	// 0 means no limit
	aascfg.rs_maxjumpfallheight		= 450;
}

static void ParseBBox(aas_bbox_t *bbox, const char *value)
{
	float r, h, z1;

	if (sscanf(value, " %f %f %f ", &r, &h, &z1) != 3 || r < 1 || h < 1)
		Error("bad bounding box: %s\n", value);

	bbox->mins[0] = -r;
	bbox->mins[1] = -r;
	bbox->mins[2] = z1;

	bbox->maxs[0] = r;
	bbox->maxs[1] = r;
	bbox->maxs[2] = z1 + h;
}

static void ParseVector(vec3_t v, const char *value)
{
	if (sscanf(value, " %f %f %f ", &v[0], &v[1], &v[2]) != 3)
		Error("bad vector: %s\n", value);
}

static void ParseSetting(const char *keyword, const char *value)
{
	int i;

	// handle the boolean settings

	if (Q_stricmp(keyword, "optimize") == 0 ||
		Q_stricmp(keyword, "optimise") == 0)
	{
		optimize = (atoi(value) <= 0) ? qfalse : qtrue;
		return;
	}

	if (Q_stricmp(keyword, "grapple") == 0 ||
		Q_stricmp(keyword, "grapplereach") == 0)
	{
		calcgrapple = (atoi(value) <= 0) ? qfalse : qtrue;
		return;
	}

	if (Q_stricmp(keyword, "forcesidesvisible") == 0)
	{
		forcesidesvisible = (atoi(value) <= 0) ? qfalse : qtrue;
		return;
	}


	// handle things in aascfg...

	if (Q_stricmp(keyword, "bbox_normal") == 0)
	{
		ParseBBox(&aascfg.bboxes[0], value);
		return;
	}
	if (Q_stricmp(keyword, "bbox_crouch") == 0)
	{
		ParseBBox(&aascfg.bboxes[1], value);
		return;
	}

	if (Q_stricmp(keyword, "phys_gravitydirection") == 0)
	{
		ParseVector(aascfg.phys_gravitydirection, value);
		return;
	}

	for (i = 0 ; cfg_fields[i].name != NULL ; i++)
	{
		if (Q_stricmp(keyword, cfg_fields[i].name) == 0)
		{
			float *var = (float *)( ((char*)&aascfg) + cfg_fields[i].offset );

			if (sscanf(value, " %f ", var) != 1)
				Error("bad %s value: %s\n", keyword, value);

			return;
		}
	}

	// ignore these keywords for compatibility
	if (Q_stricmp(keyword, "phys_maxacceleration") == 0 ||
		Q_stricmp(keyword, "rs_allowladders") == 0)
	{
		return;
	}

	// ouch, unknown keyword
	Error("unknown keyword '%s' in cfg file\n", keyword);
}

static qboolean SettingIsBoolean(const char *keyword)
{
	if (Q_stricmp(keyword, "optimize") == 0 ||
		Q_stricmp(keyword, "optimise") == 0 ||
		Q_stricmp(keyword, "grapple") == 0 ||
		Q_stricmp(keyword, "grapplereach") == 0 ||
		Q_stricmp(keyword, "forcesidesvisible") == 0)
	{
		return qtrue;
	}

	return qfalse;
}

void LoadCfgFile(const char *filename)
{
	// andrewj: wrote this new implementation.
	//
	// The idea is a bit different, this ".cfg" file can contain
	// settings for other tools and we need to skip those settings
	// and find the "aas" settings.

	char *text_p;
	const char *token;
	const char *pending = NULL;
	qboolean foundsection = qfalse;
	int bracket_level = 0;

	// this will Error() if the file does not exist
	LoadFile(filename, (void **) &text_p, 0, 0);

	Log_Print("Reading cfg file %s\n", filename);
	COM_BeginParseSession(filename);

	while (text_p || pending)
	{
		const char *keyword;
		const char *value;

		if (pending != NULL)
		{
			token = pending;
			pending = NULL;
		}
		else
		{
			token = COM_Parse(&text_p);
		}

		if (Q_stricmp(token, "{") == 0)
		{
			bracket_level++;
			continue;
		}
		else if (Q_stricmp(token, "}") == 0)
		{
			bracket_level--;

			if (bracket_level == 0)
				foundsection = qfalse;
			else if (bracket_level < 0)
				Error("mismatched {} in cfg file\n");

			continue;
		}

		if (bracket_level == 0 && Q_stricmp(token, "aas") == 0)
		{
			foundsection = qtrue;
			continue;
		}

		if (! (bracket_level == 1 && foundsection))
			continue;

		/* parse a single setting */

		keyword = Q_strdup(token);

		if (! isalpha(keyword[0]))
			Error("bad keyword '%s' in cfg file\n", keyword);

		qboolean is_bool = SettingIsBoolean(keyword);

		// for booleans, a following value must be "0" or "1" or omitted.
		// for all other settings, a following value is compulsory.

		value = NULL;

		if (text_p != NULL)
		{
			token = COM_Parse(&text_p);
			value = token; // can be NULL

			if (is_bool && token && ! (token[0] == '0' || token[0] == '1'))
			{
				// push the token back (like ungetc)
				pending = token;
				value = "1";
			}
		}

		if (value == NULL || value[0] == 0 || Q_stricmp(value, "}") == 0)
			Error("missing value for '%s' in cfg file\n", keyword);

		// this can indicate a missing value
		if (! is_bool && isalpha(value[0]))
			Error("bad or missing value for '%s' in cfg file\n", keyword);

		ParseSetting(keyword, value);

		FreeMemory((void *)keyword);
	}

	/* check stuff */

	if (VectorLength(aascfg.phys_gravitydirection) < 0.1)
		Error("invalid gravity direction in cfg file\n");

	VectorNormalize(aascfg.phys_gravitydirection);
}
