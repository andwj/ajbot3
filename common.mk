#
#  Common makefile rules for AJBOT3
#

DUMMY_FILE=$(BUILD_DIR)/zzdummy

#
# Object files
#

OBJS=\
	$(BUILD_DIR)/src/aas_areamerging.o \
	$(BUILD_DIR)/src/aas_cfg.o \
	$(BUILD_DIR)/src/aas_create.o \
	$(BUILD_DIR)/src/aas_edgemelting.o \
	$(BUILD_DIR)/src/aas_facemerging.o \
	$(BUILD_DIR)/src/aas_file.o \
	$(BUILD_DIR)/src/aas_gsubdiv.o \
	$(BUILD_DIR)/src/aas_map.o \
	$(BUILD_DIR)/src/aas_prunenodes.o \
	$(BUILD_DIR)/src/aas_store.o \
	\
	$(BUILD_DIR)/src/be_aas_bspq3.o \
	$(BUILD_DIR)/src/be_aas_cluster.o \
	$(BUILD_DIR)/src/be_aas_move.o \
	$(BUILD_DIR)/src/be_aas_optimize.o \
	$(BUILD_DIR)/src/be_aas_reach.o \
	$(BUILD_DIR)/src/be_aas_sample.o \
	$(BUILD_DIR)/src/be_misc.o \
	\
	$(BUILD_DIR)/src/brushbsp.o \
	$(BUILD_DIR)/src/csg.o \
	$(BUILD_DIR)/src/entity.o \
	$(BUILD_DIR)/src/l_cmd.o \
	$(BUILD_DIR)/src/l_dye.o \
	$(BUILD_DIR)/src/l_log.o \
	$(BUILD_DIR)/src/l_mem.o \
	$(BUILD_DIR)/src/l_poly.o \
	$(BUILD_DIR)/src/l_script.o \
	$(BUILD_DIR)/src/leakfile.o \
	$(BUILD_DIR)/src/main.o \
	$(BUILD_DIR)/src/map.o \
	$(BUILD_DIR)/src/map_q3.o \
	$(BUILD_DIR)/src/portals.o \
	$(BUILD_DIR)/src/tree.o \
	\
	$(BUILD_DIR)/qcommon/bsp.o  \
	$(BUILD_DIR)/qcommon/bsp_q3.o  \
	$(BUILD_DIR)/qcommon/cm_load.o  \
	$(BUILD_DIR)/qcommon/cm_patch.o \
	$(BUILD_DIR)/qcommon/cm_test.o  \
	$(BUILD_DIR)/qcommon/cm_trace.o \
	$(BUILD_DIR)/qcommon/md4.o \
	$(BUILD_DIR)/qcommon/q_math.o \
	$(BUILD_DIR)/qcommon/q_shared.o

$(BUILD_DIR)/src/%.o: src/%.c
	$(CC) $(CFLAGS) -Isrc $(CPPFLAGS) -o $@ -c $<

$(BUILD_DIR)/qcommon/%.o: qcommon/%.c
	$(CC) $(CFLAGS) -Iqcommon $(CPPFLAGS) -o $@ -c $<

#
# Main targets
#

all: $(DUMMY_FILE) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(BUILD_DIR)/*/*.[oa]
	rm -f core core.* ERRS

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

stripped: all
	$(STRIP) $(STRIP_FLAGS) $(PROGRAM)

# this is used to create the build directory
$(DUMMY_FILE):
	mkdir -p $(BUILD_DIR)/src
	mkdir -p $(BUILD_DIR)/qcommon
	@touch $@

.PHONY: all clean stripped

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
