/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#include "main.h"

extern int c_nodes;
static int c_pruned;


static node_t *NodeForPoint (node_t *node, vec3_t origin)
{
	plane_t	*plane;
	vec_t	d;

	while (node->planenum != PLANENUM_LEAF)
	{
		plane = &mapplanes[node->planenum];
		d = DotProduct (origin, plane->normal) - plane->dist;
		if (d >= 0)
			node = node->children[0];
		else
			node = node->children[1];
	}
	return node;
}

static void Tree_FreePortals_r (node_t *node)
{
	portal_t	*p, *nextp;
	int			s;

	// free children
	if (node->planenum != PLANENUM_LEAF)
	{
		Tree_FreePortals_r(node->children[0]);
		Tree_FreePortals_r(node->children[1]);
	}

	// free portals
	for (p = node->portals; p; p = nextp)
	{
		s = (p->nodes[1] == node);
		nextp = p->next[s];

		RemovePortalFromNode (p, p->nodes[!s]);

		FreePortal(p);
	}
	node->portals = NULL;
}

static void Tree_Free_r (node_t *node)
{
//	face_t *f, *nextf;
	bspbrush_t *brush, *nextbrush;

	//free children
	if (node->planenum != PLANENUM_LEAF)
	{
		Tree_Free_r (node->children[0]);
		Tree_Free_r (node->children[1]);
	}
	//free bspbrushes
//	FreeBrushList (node->brushlist);
	for (brush = node->brushlist; brush; brush = nextbrush)
	{
		nextbrush = brush->next;

		FreeBrush(brush);
	}
	node->brushlist = NULL;

	/*
	NOTE: only used when creating Q2 bsp
	// free faces
	for (f = node->faces; f; f = nextf)
	{
		nextf = f->next;

		FreeFace(f);
	}
	*/

	// free the node
	if (node->volume)
	{
		FreeBrush (node->volume);
	}

	c_nodes--;

	FreeMemory(node);
}

void Tree_Free(tree_t *tree)
{
	//if no tree just return
	if (!tree)
		return;

	Tree_FreePortals_r(tree->headnode);
	Tree_Free_r(tree->headnode);

	FreeMemory(tree);
}

tree_t *Tree_Alloc(void)
{
	tree_t *tree = GetClearedMemory(sizeof(*tree));

	ClearBounds (tree->mins, tree->maxs);

	return tree;
}

static void Tree_Print_r (node_t *node, int depth)
{
	int		i;
	plane_t	*plane;
	bspbrush_t	*bb;

	for (i=0 ; i<depth ; i++)
		Log_Print ("  ");

	if (node->planenum == PLANENUM_LEAF)
	{
		if (!node->brushlist)
			Log_Print ("NULL\n");
		else
		{
			for (bb=node->brushlist ; bb ; bb=bb->next)
				Log_Print ("%i ", bb->original->brushnum);
			Log_Print ("\n");
		}
		return;
	}

	plane = &mapplanes[node->planenum];
	Log_Print ("#%i (%5.2f %5.2f %5.2f):%5.2f\n", node->planenum,
		plane->normal[0], plane->normal[1], plane->normal[2],
		plane->dist);

	Tree_Print_r (node->children[0], depth+1);
	Tree_Print_r (node->children[1], depth+1);
}

// NODES THAT DON'T SEPERATE DIFFERENT CONTENTS CAN BE PRUNED
static void Tree_PruneNodes_r (node_t *node)
{
	bspbrush_t *b, *next;

	if (node->planenum == PLANENUM_LEAF) return;

	Tree_PruneNodes_r (node->children[0]);
	Tree_PruneNodes_r (node->children[1]);

	if ((node->children[0]->contents & CONTENTS_SOLID)
		&& (node->children[1]->contents & CONTENTS_SOLID))
	{
		if (node->faces)
			Error ("node->faces seperating CONTENTS_SOLID");
		if (node->children[0]->faces || node->children[1]->faces)
			Error ("!node->faces with children");
		// FIXME: free stuff
		node->planenum = PLANENUM_LEAF;
		node->contents = CONTENTS_SOLID;
		node->detail_seperator = qfalse;

		if (node->brushlist)
			Error ("PruneNodes: node->brushlist");

		// combine brush lists
		node->brushlist = node->children[1]->brushlist;

		for (b = node->children[0]->brushlist; b; b = next)
		{
			next = b->next;
			b->next = node->brushlist;
			node->brushlist = b;
		}

		//free the child nodes
		FreeMemory(node->children[0]);
		FreeMemory(node->children[1]);

		//two nodes are cut away
		c_pruned += 2;
	}
}

void Tree_PruneNodes(node_t *node)
{
	Log_Print("------- Prune Nodes --------\n");
	c_pruned = 0;
	Tree_PruneNodes_r(node);
	Log_Print("%5i pruned nodes\n", c_pruned);
}
