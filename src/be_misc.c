/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

#include "main.h"

#include "../qcommon/aasfile.h"
#include "../qcommon/cm_public.h"

#include "botlib.h"
#include "be_misc.h"
#include "be_aas.h"
#include "be_aas_def.h"
#include "be_aas_bsp.h"
#include "be_aas_cluster.h"
#include "be_aas_move.h"
#include "be_aas_reach.h"
#include "be_aas_sample.h"

clipHandle_t worldmodel;


void AAS_Error(char *fmt, ...)
{
	va_list argptr;
	char text[1024];

	va_start(argptr, fmt);
	vsprintf(text, fmt, argptr);
	va_end(argptr);

	Error("%s", text);
}

void botimport_DebugLine(vec3_t start, vec3_t end, int color)
{
}

static void AAS_ClearShownDebugLines(void)
{
}

qboolean botimport_GetEntityToken( int *offset, char *buffer, int size ) {
	return CM_GetEntityToken( offset, buffer, size );
}

void botimport_Trace(bsp_trace_t *bsptrace, vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end, int contentmask)
{
	CM_BoxTrace(bsptrace, start, end, mins, maxs, worldmodel, contentmask, TT_AABB);
}

int botimport_PointContents(vec3_t p)
{
	return CM_PointContents(p, worldmodel);
}

static void *botimport_GetMemory(int size)
{
	return GetMemory(size);
}

void botimport_Print(int type, char *fmt, ...)
{
	va_list argptr;
	char buf[1024];

	va_start(argptr, fmt);
	Q_vsnprintf(buf, sizeof(buf), fmt, argptr);
	va_end(argptr);

	// ignore any progress messages
	if (buf[0] == '\r')
		return;

	// since PRT_ERROR messages should not abort, downgrade to a warning.
	if (type >= PRT_WARNING) {
		Warning("%s", buf);
	} else {
		Log_Print("%s", buf);
	}
}

void botimport_BSPModelMinsMaxsOrigin(int modelnum, vec3_t angles, vec3_t outmins, vec3_t outmaxs, vec3_t origin)
{
	clipHandle_t h;
	vec3_t mins, maxs;
	float max;
	int	i;

	h = CM_InlineModel(modelnum);
	CM_ModelBounds(h, mins, maxs);
	//if the model is rotated
	if ((angles[0] || angles[1] || angles[2]))
	{	// expand for rotation

		max = RadiusFromBounds(mins, maxs);
		for (i = 0; i < 3; i++)
		{
			mins[i] = (mins[i] + maxs[i]) * 0.5 - max;
			maxs[i] = (mins[i] + maxs[i]) * 0.5 + max;
		}
	}
	if (outmins) VectorCopy(mins, outmins);
	if (outmaxs) VectorCopy(maxs, outmaxs);
	if (origin) VectorClear(origin);
}

void AAS_CalcReachAndClusters(const char *filename)
{
	Log_Print("loading collision map...\n");

	//load the map
	CM_LoadMap(filename, qfalse, &aasworld.bspchecksum);
	//get a handle to the world model
	worldmodel = CM_InlineModel(0);		// 0 = world, 1 + are bmodels
	//load the BSP entity string
	AAS_LoadBSPFile();
	//initialize AAS link heap
	AAS_InitAASLinkHeap();
	//initialize the AAS linked entities for the new map
	AAS_InitAASLinkedEntities();
	//reset all reachabilities and clusters
	aasworld.reachabilitysize = 0;
	aasworld.numclusters = 0;
	//set all view portals as cluster portals in case we re-calculate the reachabilities and clusters (with -reach)
	AAS_SetViewPortalsAsClusterPortals();
	//calculate reachabilities
	AAS_InitReachability();
	//calculate clusters
	AAS_InitClustering();
}
