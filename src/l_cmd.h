/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/
// cmdlib.h

#ifndef __CMDLIB__
#define __CMDLIB__

#define Q_rint(x)  floor((x) + 0.5)


int Q_filelength (FILE *f);

double I_FloatTime (void);

void Error(char *error, ...)  __attribute__ ((noreturn, format (printf, 1, 2)));
void Warning(char *warning, ...) __attribute__ ((format (printf, 1, 2)));

int LoadFile (const char *filename, void **bufferptr, int offset, int length);
qboolean FileExists (const char *filename);

void ExtractFileBase (char *path, char *dest);

//append a path separator to the given path not exceeding the length
void AppendPathSeparator(char *path, int length);

char *Q_strdup(const char *s);


void CRC_Init(unsigned short *crcvalue);
void CRC_ProcessByte(unsigned short *crcvalue, byte data);
unsigned short CRC_Value(unsigned short crcvalue);


// for compression routines
typedef struct
{
	byte	*data;
	int		count;
} cblock_t;

#endif

