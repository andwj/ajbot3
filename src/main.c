/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

#ifdef _WIN32
#include <direct.h>
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <unistd.h>
#include <glob.h>
#include <sys/stat.h>
#endif

#include "main.h"

#include "../qcommon/aasfile.h"

#include "aas_create.h"
#include "aas_store.h"
#include "aas_file.h"
#include "aas_cfg.h"
#include "be_aas_optimize.h"
#include "botlib.h"
#include "be_misc.h"


#define AJ_PARSE_ARG_IMPL
#include "parse_arg.h"


qboolean	optimize;			// enable optimisation
qboolean	calcgrapple;		// perform grapple-hook reachabilities
qboolean	forcesidesvisible;	// force all brush sides to be visible when loaded from bsp


static int MatchBool(const char *long_name, const char *short_name, qboolean *var)
{
	if (aj_arg_Match(long_name, short_name))
	{
		*var = qtrue;
		return 1;
	}

	return 0;
}

static void ShowUsage(void)
{
	printf("\nUsage: ajbot3 [-bsp2aas] FILE.bsp [OPTIONS...]\n"
		"\n"
		"Options:\n"
		"  -output   <filename>    : set output filename\n"
		"  -log      <filename>    : set log filename, \"none\" to disable\n"
		"  -cfg      <filename>    : load this cfg file\n"
		"  -verbose                : verbose output on terminal\n"
		"  -extra                  : enable messages with extra details\n"
		"\n");
}

static void WriteOptionSummary(void)
{
	Log_Write("Options:\n");
	Log_Write("  optimize %d\n", optimize ? 1 : 0);
	Log_Write("  grapple %d\n", calcgrapple ? 1 : 0);
	Log_Write("  forcesidesvisible %d\n", forcesidesvisible ? 1 : 0);
}

int AASInfoMain(const char *aasfile)
{
	if (!AAS_LoadAASFile(aasfile))
		Error("error loading aas file %s\n", aasfile);

	// TODO : show even *more* information than this
	AAS_ShowTotals();

	return 0;
}

int main(int argc, const char **argv)
{
	const char *inputpath  = NULL;
	const char *outputpath = NULL;
	const char *logfile = NULL;
	const char *cfgfile = NULL;

	static char inputbuf[MAX_OSPATH]  = "";
	static char outputbuf[MAX_OSPATH] = "";
	static char logbuf[MAX_OSPATH] = "";
	static char cfgbuf[MAX_OSPATH] = "";

	double start_time;


	Swap_Init();

	Log_Message("AJBOT3 version "AJBOT3_VERSION" (%s)\n", __DATE__);

	DefaultCfg();

	aj_arg_Init(argc, argv);

	if (aj_arg_Done())
	{
		ShowUsage();
		return 0;
	}

	while (! aj_arg_Done())
	{
		// a bare filename?
		if (! aj_arg_CheckOption())
		{
			if (inputpath || inputbuf[0])
				Error("multiple filenames specified.\n");

			Q_strncpyz(inputbuf, aj_arg_Remove(), sizeof(inputbuf));
			continue;
		}

		// this is mainly for backwards compatibility
		if (aj_arg_MatchString("-bsp2aas", "", &inputpath))
			continue;

		// this is a separate utility
		if (aj_arg_MatchString("-aasinfo", "", &inputpath))
		{
			return AASInfoMain(inputpath);
		}

		if (aj_arg_Match("-help", "-h"))
		{
			ShowUsage();
			return 0;
		}

		if (aj_arg_Match("-version", ""))
		{
			// nothing else needed, name and version already shown
			return 0;
		}

		if (aj_arg_MatchString("-output", "-o", &outputpath))
			continue;

		if (aj_arg_MatchString("-log", "", &logfile))
			continue;

		if (aj_arg_MatchString("-cfg", "", &cfgfile))
			continue;

		if (MatchBool("-verbose", "-v", &verbose))
			continue;

		if (MatchBool("-extra", "-e", &extra))
			continue;

		// unknown option or missing value
		const char *err_msg = aj_arg_GetError();
		Error("%s\n", err_msg);
	}

	// do we have a file to process?
	if (inputpath && inputbuf[0])
		Error("multiple filenames specified.\n");

	if (! inputpath)
		inputpath = inputbuf;

	if (strlen(inputpath) == 0)
		Error("no input files\n");

	if (! FileExists(inputpath))
		Error("no such file: %s\n", inputpath);

	// determine output filename...
	if (! outputpath)
		outputpath = outputbuf;

	if (strlen(outputpath) == 0)
	{
		strcpy(outputbuf, inputpath);
		COM_SetExtension(outputbuf, MAX_OSPATH, ".aas");
		outputpath = outputbuf;
	}

	// determine log filename, "none" to disable logging
	if (logfile == NULL)
	{
		strcpy(logbuf, outputpath);
		COM_SetExtension(logbuf, MAX_OSPATH, ".log");
		logfile = logbuf;
	}
	else if (Q_stricmp(logfile, "none") == 0 || Q_stricmp(logfile, "/dev/null") == 0)
	{
		logfile = NULL;
	}

	// determine cfg filename, "none" to disable it
	if (cfgfile == NULL)
	{
		strcpy(cfgbuf, inputpath);
		COM_SetExtension(cfgbuf, MAX_OSPATH, ".cfg");
		cfgfile = cfgbuf;

		// it is fine when an implicit cfg file does not exist
		if (! FileExists(cfgfile))
			cfgfile = NULL;
	}
	else if (Q_stricmp(cfgfile, "none") == 0 || Q_stricmp(cfgfile, "/dev/null") == 0)
	{
		cfgfile = NULL;
	}


	start_time = I_FloatTime();

	if (logfile)
		Log_Open(logfile);

	// we showed this previously on the terminal, this one is log-only
	Log_Write("AJBOT3 version "AJBOT3_VERSION" (%s)\n", __DATE__);

	if (cfgfile != NULL)
		LoadCfgFile(cfgfile);

	// this is consistent with AJMAP3, helps visual scanning of messages
	Log_Message("--- AAS ---\n");

	WriteOptionSummary();


	Log_Message("Reading %s\n", inputpath);

	LoadMapFromBSP(inputpath);

	Log_Message("Creating AAS...\n");

	//create the AAS file
	AAS_Create(outputpath);

	Log_Message("Reachability...\n");

	//calculate the reachabilities and clusters
	AAS_CalcReachAndClusters(inputpath);

	if (optimize)
		AAS_Optimize();

	Log_Message("Writing %s\n", outputpath);

	//write out the stored AAS file
	if (!AAS_WriteAASFile(outputpath))
		Error("error writing %s\n", outputpath);

	//deallocate memory
	AAS_FreeMaxAAS();

	Log_Write("\n");
	Log_Write("======== %1.0f seconds elapsed ========================================\n",
		I_FloatTime() - start_time + 0.9);

	Log_Message("\n");
	Log_Close();

	return 0;
}
