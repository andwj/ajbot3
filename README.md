
AJBOT3
======

by Andrew Apted, 2020.


About
-----

AJBOT3 is a command-line tool for producing the ".aas" file
which the Quake3 engine needs to allow bots to navigate a map.
The code is based on the "bspc" program, but with some fairly
significant changes so I have given this a distinct name to
avoid confusion.


Differences to BSPC
-------------------

* codebase is stand-alone, not part of the engine source
* cleaner, safer and more readable code
* supports CFG files associated with a map
* better terminal output with colored text
* better logging, can specify log file or disable it altogether
* only builds AAS files (no other junk except `-aasinfo`)
* only supports Quake 3 (not Quake 1 or 2 or HL)


Website
-------

https://gitlab.com/andwj/ajbot3


Binary Packages
---------------

https://gitlab.com/andwj/ajbot3/tags/v3.1.0

Only Windows has binary packages, for Linux (etc) you will need to
clone this repository and compile the source code.


Legalese
--------

AJBOT3 is Free Software, under the terms of the GNU General Public
License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

AJBOT3 comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.


Acknowledgements
----------------

Jan Paul van Waveren : designed the Quake III Arena bots and the
Area Awareness System (AAS), and created the BSPC program.

Ben Noordhuis : made a stand-alone version of the BSPC code with
numerous fixes.  AJBOT3 began as a fork of this codebase.
See: https://github.com/bnoordhuis/bspc

Zack Middleton : created the Spearmint engine, and made numerous
fixes to the BSPC code which I have incorporated into AJBOT3.
See: https://github.com/zturtleman/spearmint

Id Software : created Quake III Arena and released the code for
the engine and various tools under a free software license.


Compiling
---------

On Linux, just type "make".

On BSD systems, just type "gmake" (GNU make is required).

The Windows binary is built on Linux using a cross compiler,
and there is currently no support for building natively.


Usage
-----

Running AJBOT3 without any arguments, or using the `-h` or
`--help` options, will show a brief summary of the available
command-line options.  Options begin with either a single or
double dash (`-`) character, and can appear on the command-line
in any order.

Normal usage is to just specify the filename of a `.bsp` file,
and AJBOT3 will create a corresponding `.aas` file.  Only a
single file can be processed at a time.

Note that if you want to control certain features, such as
enabling support for the grapple hook, then you need to use
a map-specific config file (see the next section).

The following options are available:

`-bsp2aas`  
This is provided for backwards compatibility.  It is the
default behavior of AJBOT3 to build an AAS file from an
existing BSP file.

`-aasinfo  FILENAME`  
Dump some information about an AAS file.

`-output  FILENAME`  
The default output filename is the same as the input filename
with the `.bsp` extension changed to `.aas`, however this
option lets you explicitly specify the output filename.

`-log  FILENAME`  
The default log filename is the same as the input filename
with the extension changed to `.log`, however this option lets
you specify the log filename directly.  The keyword `none` is
also accepted and will inhibit the creation of a log file.

`-cfg  FILENAME`  
The normal CFG filename is the same as the input filename
with the extension changed to `.cfg`, however this option lets
you specify a different filename.  You will get a fatal error
if this file cannot be found, unlike the normal behavior where
the implicitly determined config file is allowed to be absent.

`-verbose`  
Enable more verbose output on the terminal.  Normally only
important messages are shown on the terminal, but this option
causes everything which is written to the log file to also
be displayed.  Note that `-v` is also accepted.

`-extra`  
Enable extra messages containing extra detail about what is
happening during the build of the AAS file.  Many of these
messages are about minor issues which generally don't affect
the usability of the generated AAS file, though sometimes
they can indicate a serious problem in your map.


Config Files
------------

Config files are a feature of the AJMAP3 and AJBOT3 tools.
Each `.map` file can have a config file associated with it,
typically it is the same filename with the `.cfg` extension
(though a different file can be specified via a command-line
option).  These files contain settings to control how each
part of the map compiling process operates.

The syntax of this file is quite simple, it consists of one
or more sections which is introduced by a keyword and is
followed by a `{` opening brace, then a group of settings,
and is terminated by a `}` closing brace.  Each setting is
a keyword followed by a value, however for boolean settings
the value can be omitted ("1" will be assumed, since most
boolean settings have a default of "0" i.e. disabled).
Comments begin with `//` and continue to the end of the
line, and are completely ignored.

An example CFG file:
```
    bsp
    {
    }

    light
    {
       fast
       samples 2
       bounce 1
    }

    aas
    {
       grapple
    }
```

For AJBOT3, only the `aas` section is read, everything else
is ignored.  This section can contain two types of settings:
general settings like enabling the grappling hook, and game
settings which specify the physics of the target game or mod.
The game settings have the `bbox_`, `phys_` or `rs_` prefixes,
and should not be used unless you are developing your own game
or mod with different physics than standard Quake III Arena.
Their meanings are beyond the scope of this README document.


General Config Settings
-----------------------

The following are the general settings which can be used in
the `aas` section of a CFG file:

`grapple`  
Enables support for the grapple hook, finding places where the
bot may use the grapple hook to reach other parts of the map.
For compatibility, the name `grapplereach` is also accepted.
Apparently this feature is buggy in the vanilla Quake 3 engine,
plus it can significantly increase the size of the AAS file,
so it is disabled by default.

`optimize`  
Enables an optimization pass which reduces the size of the
AAS file by omitting faces, edges and vertices which are
determined to be unnecessary.  This can make the AAS file
much smaller.  I personally don't know if this can cause
any problems or not, hence it is disabled by default.
The alternative spelling `optimise` is also accepted.

`forcesidesvisible`  
Changes the way brushes and sides of brushes are handled when
performing BSP and CSG operations.  Normally brushes without
a textured side are skipped as potential BSP splitters, and
brushes without a visible side are skipped in the CSG phase,
however this setting forces them to be handled.

Personally I don't understand why this setting is necessary.
I think it may be related to building the bsp file with certain
settings enabled, perhaps `meta` or `patchMeta`, and that confuses
the AAS tool.  I suggest not using the `forcesidesvisible` setting
unless you are really sure you need it.

