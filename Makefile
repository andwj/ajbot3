#
#  --- AJBOT3 ---
#
#  Makefile for Linux and BSD systems.
#  Requires GNU make.
#

PROGRAM=ajbot3

# CC=i686-w64-mingw32-gcc
# CC=clang
# CC=tcc

STRIP=strip

WARNINGS=-Wall -Wextra -Wno-unused-function -Wno-unused-parameter
OPTIMISE=-O2 -g
STRIP_FLAGS=--strip-unneeded

# default flags for compiler, preprocessor and linker
CFLAGS ?= $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

# miscellaneous needed things
CPPFLAGS += -DQDECL= -DBSPC

LIBS += -lm
LDFLAGS += -Wl,--warn-common

# uncomment this to build statically linked binaries
# LDFLAGS += -static

# set directory where object files are stored
BUILD_DIR=build

include common.mk

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
