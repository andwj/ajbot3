/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

struct aas_clientmove_s;
struct aas_areainfo_s;
struct aas_altroutegoal_s;
struct aas_predictroute_s;

//debug line colors
#define LINECOLOR_NONE			0
#define LINECOLOR_RED			1
#define LINECOLOR_GREEN			2
#define LINECOLOR_BLUE			3
#define LINECOLOR_YELLOW		4
#define LINECOLOR_ORANGE		5
#define LINECOLOR_CYAN  		6

//Print types
#define PRT_DEVELOPER			0
#define PRT_MESSAGE				1
#define PRT_WARNING				2
#define PRT_ERROR				3
#define PRT_FATAL				4
#define PRT_EXIT				5

typedef trace_t bsp_trace_t;

extern int bot_developer;

// andrewj: this is what's left of "botimport"
qboolean botimport_GetEntityToken( int *offset, char *buffer, int size );
void botimport_Trace(bsp_trace_t *bsptrace, vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end, int contentmask);
int botimport_PointContents(vec3_t p);
void botimport_Print(int type, char *fmt, ...);
void botimport_BSPModelMinsMaxsOrigin(int modelnum, vec3_t angles, vec3_t outmins, vec3_t outmaxs, vec3_t origin);
void botimport_DebugLine(vec3_t start, vec3_t end, int color);

// andrewj: removed heaps of stuff not needed for AJBOT3

