/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

extern qboolean verbose;
extern qboolean extra;

//open a log file
void Log_Open(const char *filename);
//close the current log file
void Log_Close(void);
//close log file if present
void Log_Shutdown(void);
//print on stdout (always) and write to the current log file
void Log_Message(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
//print on stdout if -verbose, always write to the current log file
void Log_Print(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
//same as Log_Print but only when "extra" is enabled
void Log_Extra(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
//write to the current opened log file
void Log_Write(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
//flush log file
void Log_Flush(void);

// andrewj: added this API for progress messages.
void Progress_Begin(const char *what, int total, int skip);
void Progress_Update(int along);
void Progress_End();

