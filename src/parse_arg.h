
/**********************
  AJ ARGUMENT PARSING
***********************

by Andrew Apted, March 2019


The code herein is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this code.  Use at your own risk.

Permission is granted to anyone to use this code for any purpose,
including commercial applications, and to freely redistribute and
modify it.  An acknowledgement would be nice, but is not required.

_____

ABOUT
_____

This is a simple header-file library for C and C++ programs to
parse command-line arguments.  It supercedes a similar lib
(but very different API) which I wrote years ago.

In the file which you want to contain the implementation, add the
following *before* the inclusion of this header :

    #define AJ_PARSE_ARG_IMPL

____________

DEPENDENCIES
____________

No other libraries.

_____

USAGE
_____

// Init() prepares to handle the arguments.  Just give it the
// same parameters as your main() function.
//
// NOTE: the string pointers are used 'as-is' -- we assume they
// will never be free or modified until the program ends.

aj_arg_Init(int argc, const char **argv)


// Rewind() allows iterating a second (or third, etc) time
// through the arguments.  Any arguments removed earlier will
// not be seen again.

aj_arg_Rewind()


// Done() returns 1 if there are no more arguments, 0 otherwise.

aj_arg_Done()


// CheckOption() checks if the current argument is actually
// an option (rather than a normal value or filename).  It
// returns 1 if it *IS* an option (i.e. it begins with '-'),
// otherwise 0.
//
// For options, the error string is setup to say that the
// option is unknown, otherwise it is cleared to "".
//
// This does not advance the current arg pointer, so you
// need to call Remove() or Skip() when this returns zero.

aj_arg_CheckOption()


// GetError() returns the current error string.  This is set
// by CheckOption() to an unknown option message (or "" for
// non-options), and is updated by MatchXXX() calls if the
// option name matches but the parameter could not be parsed.
// You typically call this after trying all your matches.

aj_arg_GetError()


// Remove() removes the current argument and returns it,
// The current pointer is advanced to the next one.
// If the argument was an option, then any following values
// are also removed (anything not an option).

aj_arg_Remove()


// Skip() advances the current pointer without removing the
// argument.  If the argument was an option, then Skip() also
// skips any following values (anything not an option).
//
// This function is only useful when doing two or more passes
// over the argument list.  For normal (single pass) code,
// just use Remove() instead.

aj_arg_Skip()


// Match() checks if the current argument matches the given
// option name.  When it matches, the argument is consumed and
// the function returns 1, otherwise 0 is returned and no state
// has changed.
//
// The 'long_name' is a string and must include the "-" prefix.
// A double "--" prefix is handled automatically and does not
// not to be explicitly checked.  It can be NULL or "" to
// disable the long version.
//
// The 'short_name' is also a string but should be a single
// letter following the '-' prefix.  It can also be NULL or ""
// to disable the short version.

aj_arg_Match(long_name, short_name)


// MatchString() etc... are like Match() but expects a single
// value after the option.  If the next argument is missing
// (e.g. because it is an option) then the error string is
// updated, and the match fails.  Similarly if the value could
// not be parsed.
//
// When the match succeeds, then both the option and the next
// argument are consumed (removed) and the current pointer
// is advanced.  When it fails, the current pointer is NOT
// advanced, so you need to call Remove() or Skip() some time
// afterwards -- typically after trying all your matches.
//
// The 'var' parameter can be NULL to ignore the value.

aj_arg_MatchString(long_name, short_name, const char **var)
aj_arg_MatchInt(long_name, short_name, int *var)
aj_arg_MatchFloat(long_name, short_name, float *var)

*/


/* ---------------- API ------------------ */

#ifndef __AJ_PARSE_ARGUMENTS_H__
#define __AJ_PARSE_ARGUMENTS_H__

#ifdef __cplusplus
extern "C" {
#endif

void aj_arg_Init(int argc, const char **argv);
void aj_arg_Rewind(void);
int  aj_arg_Done(void);
int  aj_arg_CheckOption(void);
const char *aj_arg_GetError(void);
const char *aj_arg_Remove(void);
void aj_arg_Skip(void);
int aj_arg_Match(const char *long_name, const char *short_name);
int aj_arg_MatchString(const char *long_name, const char *short_name, const char **var);
int aj_arg_MatchInt(const char *long_name, const char *short_name, int *var);
int aj_arg_MatchFloat(const char *long_name, const char *short_name, float *var);

#ifdef __cplusplus
}
#endif

#endif /* __AJ_PARSE_ARGUMENTS_H__ */


/* -------------- IMPLEMENTATION ---------------- */

#ifdef AJ_PARSE_ARG_IMPL

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>

// case insensitive comparison function
#ifndef AJ_ARG_COMPARE
#define AJ_ARG_COMPARE(s1, s2)  aj_arg_CaseCmp(s1, s2)
#endif


struct ajarg__node
{
	const char *str;
	struct ajarg__node *next;
};

struct ajarg__node *ajarg__list;
struct ajarg__node *ajarg__cur;
struct ajarg__node *ajarg__prev;

char ajarg__error_msg[1024];


static void aj_arg_AddToTail(struct ajarg__node *nd)
{
	struct ajarg__node *prev;

	if (ajarg__list == NULL)
	{
		ajarg__list = nd;
		nd->next = NULL;
		return;
	}

	for (prev = ajarg__list ; prev->next != NULL ; prev = prev->next)
	{ }

	prev->next = nd;
}


void aj_arg_Init(int argc, const char **argv)
{
	int i;
	struct ajarg__node *nd;

	// ignore the program name
	argv++;
	argc--;

	ajarg__list = NULL;
	ajarg__cur  = NULL;
	ajarg__prev = NULL;

	if (argc == 0)
		return;

	nd = calloc(argc, sizeof(struct ajarg__node));

	if (nd == NULL)
	{
		// a bit rude, but unlikely to occur
		abort();
		return;
	}

	for (i = 0 ; i < argc ; i++)
	{
		const char *cur = argv[i];

		// a NULL?  WTF?
		if (! cur)
			continue;

#ifdef __APPLE__
		// ignore MacOS X rubbish
		if (cur[0] == '-' && cur[1] == 'p' &&
			cur[2] == 's' && cur[3] == 'n')
			continue;
#endif
		nd->str = cur;

		aj_arg_AddToTail(nd);

		nd++;
	}

	// begin at the beginning
	ajarg__cur  = ajarg__list;
	ajarg__prev = NULL;
}


void aj_arg_Rewind(void)
{
	ajarg__cur  = ajarg__list;
	ajarg__prev = NULL;
}


int aj_arg_Done(void)
{
	return (ajarg__cur == NULL);
}


const char *aj_arg_GetError(void)
{
	return ajarg__error_msg;
}


static int aj_arg_IsOption(struct ajarg__node *nd)
{
	const char *str;

	if (nd == NULL)
		return 0;

	str = nd->str;

	if (str[0] != '-')
		return 0;

	// treat negative numbers as NOT options
	if (isdigit(str[1]))
		return 0;

	return 1;
}


int aj_arg_CheckOption(void)
{
	ajarg__error_msg[0] = 0;

	if (ajarg__cur == NULL)
		return 0;

	int is_option = aj_arg_IsOption(ajarg__cur);

	if (! is_option)
		return 0;

	snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "unknown option: %s", ajarg__cur->str);
	ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;

	return 1;
}


static void aj_arg_RawRemove(void)
{
	if (ajarg__cur == NULL)
		return;

	ajarg__cur = ajarg__cur->next;

	if (ajarg__prev == NULL)
		ajarg__list = ajarg__cur;
	else
		ajarg__prev->next = ajarg__cur;
}


const char *aj_arg_Remove(void)
{
	const char *str;
	int is_option;

	if (ajarg__cur == NULL)
		return "";

	str = ajarg__cur->str;
	is_option = aj_arg_IsOption(ajarg__cur);

	aj_arg_RawRemove();

	// also remove any values after an option
	if (is_option)
	{
		while (ajarg__cur != NULL && !aj_arg_IsOption(ajarg__cur))
		{
			aj_arg_RawRemove();
		}
	}

	return str;
}


static void aj_arg_RawSkip(void)
{
	if (ajarg__cur == NULL)
		return;

	ajarg__prev = ajarg__cur;
	ajarg__cur  = ajarg__cur->next;
}


void aj_arg_Skip(void)
{
	int is_option = aj_arg_IsOption(ajarg__cur);

	aj_arg_RawSkip();

	// also skip any values after an option
	if (is_option)
	{
		while (ajarg__cur != NULL && !aj_arg_IsOption(ajarg__cur))
		{
			aj_arg_RawSkip();
		}
	}
}


static int aj_arg_CaseCmp(const char *A, const char *B)
{
	for (; *A || *B; A++, B++)
	{
		// this test also catches end-of-string conditions
		if (toupper(*A) != toupper(*B))
			return (toupper(*A) - toupper(*B));
	}

	return 0;
}


static int aj_arg_MatchesName(const char *long_name, const char *short_name)
{
	const char *str;

	if (ajarg__cur == NULL)
		return 0;

	str = ajarg__cur->str;

	if (short_name && short_name[0] == '-' && strlen(short_name) == 2)
	{
		if (AJ_ARG_COMPARE(str, short_name) == 0)
			return 1;
	}

	if (long_name && long_name[0] == '-')
	{
		// support GNU-style long options
		if (str[0] == '-' && str[1] == '-')
			str++;

		if (long_name[0] == '-' && long_name[1] == '-')
			long_name++;

		if (AJ_ARG_COMPARE(str, long_name) == 0)
			return 1;
	}

	return 0;
}


int aj_arg_Match(const char *long_name, const char *short_name)
{
	if (! aj_arg_MatchesName(long_name, short_name))
		return 0;

	aj_arg_RawRemove();

	return 1;
}


int aj_arg_MatchString(const char *long_name, const char *short_name, const char **var)
{
	struct ajarg__node *value;

	if (! aj_arg_MatchesName(long_name, short_name))
		return 0;

	value = ajarg__cur->next;

	if (value && aj_arg_IsOption(value))
		value = NULL;

	if (! value)
	{
		snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "missing value after %s option", ajarg__cur->str);
		ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;
		return 0;
	}

	if (var)
		*var = value->str;

	aj_arg_RawRemove();
	aj_arg_RawRemove();

	return 1;
}


int aj_arg_MatchInt(const char *long_name, const char *short_name, int *var)
{
	struct ajarg__node *value;
	int dummy_val;
	char *endptr;

	if (! aj_arg_MatchesName(long_name, short_name))
		return 0;

	value = ajarg__cur->next;

	if (value && aj_arg_IsOption(value))
		value = NULL;

	if (! value)
	{
		snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "missing value after %s option", ajarg__cur->str);
		ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;
		return 0;
	}

	if (! var)
		var = &dummy_val;

	// convert to integer, checking for errors
	errno = 0;

	*var = strtol(value->str, &endptr, 0);

	if (errno != 0 || endptr == value->str)
	{
		snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "bad value after %s option", ajarg__cur->str);
		ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;
		return 0;
	}

	aj_arg_RawRemove();
	aj_arg_RawRemove();

	return 1;
}


int aj_arg_MatchFloat(const char *long_name, const char *short_name, float *var)
{
	struct ajarg__node *value;
	float dummy_val;
	char *endptr;

	if (! aj_arg_MatchesName(long_name, short_name))
		return 0;

	value = ajarg__cur->next;

	if (value && aj_arg_IsOption(value))
		value = NULL;

	if (! value)
	{
		snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "missing value after %s option", ajarg__cur->str);
		ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;
		return 0;
	}

	if (! var)
		var = &dummy_val;

	// convert to float, and check for errors
	errno = 0;

	*var = strtof(value->str, &endptr);

	if (errno != 0 || endptr == value->str)
	{
		snprintf(ajarg__error_msg, sizeof(ajarg__error_msg), "bad value after %s option", ajarg__cur->str);
		ajarg__error_msg[sizeof(ajarg__error_msg)-1] = 0;
		return 0;
	}

	aj_arg_RawRemove();
	aj_arg_RawRemove();

	return 1;
}


#endif  /* AJ_PARSE_ARG_IMPL */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
