/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

#include "main.h"

// only print to terminal if in verbose mode
qboolean verbose = qfalse;

// control whether extra-detail messages are seen
qboolean extra = qfalse;


typedef struct logfile_s
{
	char filename[MAX_OSPATH];
	FILE *fp;
} logfile_t;

static logfile_t logfile;

static void Progress_ClearLine();


void Log_Open(const char *filename)
{
	if (!filename || !strlen(filename))
	{
		//??  printf("openlog <filename>\n");
		return;
	}
	if (logfile.fp)
	{
		printf("log file %s is already opened\n", logfile.filename);
		return;
	}

	// andrewj: open in TEXT mode, so that Windows gets \r\n line endings
	logfile.fp = fopen(filename, "a");
	if (!logfile.fp)
	{
		printf("can't open log file %s\n", filename);
		return;
	}

	Q_strncpyz(logfile.filename, filename, (int)sizeof(logfile.filename));
}

void Log_Close(void)
{
	if (logfile.fp)
	{
		fclose(logfile.fp);
		logfile.fp = NULL;
	}
}

void Log_Shutdown(void)
{
	Log_Close();
}

void Log_Message(const char *fmt, ...)
{
	va_list ap;
	char buf[2048];
	qboolean didcolor = qfalse;

	va_start(ap, fmt);
	Q_vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	// always print on the terminal
	Progress_ClearLine();

	// andrewj: colorize warning and error messages
	if (strncmp(buf, "WARNING", 7) == 0)
	{
		dyefg(stdout, DYE_YELLOW);
		didcolor = qtrue;
	}
	else if (strncmp(buf, "ERROR", 5) == 0)
	{
		dyefg(stdout, DYE_RED);
		didcolor = qtrue;
	}

	printf("%s", buf);
	fflush(stdout);

	if (didcolor)
		dyefg(stdout, DYE_RESET);

	if (logfile.fp)
	{
		fprintf(logfile.fp, "%s", buf);
		fflush(logfile.fp);
	}
}

void Log_Print(const char *fmt, ...)
{
	va_list ap;
	char buf[2048];

	va_start(ap, fmt);
	Q_vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	if (verbose)
	{
		Log_Message("%s", buf);
		return;
	}

	if (logfile.fp)
	{
		fprintf(logfile.fp, "%s", buf);
		fflush(logfile.fp);
	}
}

void Log_Extra(const char *fmt, ...)
{
	va_list ap;
	char buf[2048];

	if (! extra)
		return;

	va_start(ap, fmt);
	Q_vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	if (verbose)
	{
		Progress_ClearLine();
		printf("%s", buf);
	}

	if (logfile.fp)
	{
		fprintf(logfile.fp, "%s", buf);
		fflush(logfile.fp);
	}
}

void Log_Write(const char *fmt, ...)
{
	va_list ap;
	char buf[2048];

	if (!logfile.fp)
		return;

	va_start(ap, fmt);
	Q_vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	fprintf(logfile.fp, "%s", buf);
	fflush(logfile.fp);
}

void Log_Flush(void)
{
	if (logfile.fp)
		fflush(logfile.fp);
}

//----------------------------------------------------------------------

// andrewj: added this API for progress messages.

static const char *prog_what;
static int prog_total;
static int prog_skip;
static int prog_last_shown;

static void Progress_ClearLine()
{
	if (prog_what)
	{
		fprintf(stderr, "                                                  \r");
	}
}

// total can be zero for "unknown" (then skip comes into play).
void Progress_Begin(const char *what, int total, int skip)
{
	prog_what = what;
	prog_total = total;
	prog_skip = skip;
	prog_last_shown = -1;

	// ensure messages on stdout have been shown
	fflush(stdout);
}

void Progress_End()
{
	Progress_ClearLine();

	prog_what = NULL;
	prog_total = 0;
	prog_last_shown = -1;
}

void Progress_Update(int along)
{
	if (prog_total > 0)
	{
		int percent = (int)(100.4 * (double)along / (double)prog_total);
		if (percent == prog_last_shown)
			return;

		prog_last_shown = percent;

		fprintf(stderr, "%6d%% %s   \r", percent, prog_what);
	}
	else
	{
		if (prog_skip > 0)
		{
			int div = along / prog_skip;
			if (div == prog_last_shown)
				return;

			prog_last_shown = div;
		}

		fprintf(stderr, "%6d %s    \r", along, prog_what);
	}
}
