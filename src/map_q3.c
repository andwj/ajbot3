/*
===========================================================================
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

This file is part of Spearmint Source Code.

Spearmint Source Code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

Spearmint Source Code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Spearmint Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, Spearmint Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following
the terms and conditions of the GNU General Public License.  If not, please
request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional
terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
Suite 120, Rockville, Maryland 20850 USA.
===========================================================================
*/

#include "main.h"

#include "../qcommon/aasfile.h"	//aas_bbox_t
#include "../qcommon/cm_patch.h"
#include "../qcommon/surfaceflags.h"

#include "map_q3.h"
#include "aas_store.h"			//AAS_MAX_BBOXES
#include "aas_cfg.h"
#include "aas_map.h"			//AAS_CreateMapBrushes

#define BBOX_NORMAL_EPSILON		0.0001
#define WCONVEX_EPSILON			0.5

#define NODESTACKSIZE		1024

typedef struct cname_s
{
	int value;
	char *name;
} cname_t;

#define STRINGIZE(x) x,#x

cname_t contentnames[] =
{
	{ STRINGIZE( CONTENTS_SOLID ) },
	{ STRINGIZE( CONTENTS_LAVA ) },
	{ STRINGIZE( CONTENTS_SLIME ) },
	{ STRINGIZE( CONTENTS_WATER ) },
	{ STRINGIZE( CONTENTS_FOG ) },
	{ STRINGIZE( CONTENTS_NOTTEAM1 ) },
	{ STRINGIZE( CONTENTS_NOTTEAM2 ) },
	{ STRINGIZE( CONTENTS_NOBOTCLIP ) },
	{ STRINGIZE( CONTENTS_AREAPORTAL ) },
	{ STRINGIZE( CONTENTS_PLAYERCLIP ) },
	{ STRINGIZE( CONTENTS_MONSTERCLIP ) },

	{ STRINGIZE( CONTENTS_TELEPORTER ) },
	{ STRINGIZE( CONTENTS_JUMPPAD ) },
	{ STRINGIZE( CONTENTS_CLUSTERPORTAL ) },
	{ STRINGIZE( CONTENTS_DONOTENTER ) },
	{ STRINGIZE( CONTENTS_BOTCLIP ) },
	{ STRINGIZE( CONTENTS_MOVER ) },

	{ STRINGIZE( CONTENTS_ORIGIN ) },

	{ STRINGIZE( CONTENTS_BODY ) },
	{ STRINGIZE( CONTENTS_CORPSE ) },
	{ STRINGIZE( CONTENTS_DETAIL ) },
	{ STRINGIZE( CONTENTS_STRUCTURAL ) },
	{ STRINGIZE( CONTENTS_TRANSLUCENT ) },
	{ STRINGIZE( CONTENTS_TRIGGER ) },
	{ STRINGIZE( CONTENTS_NODROP ) },

	{0, 0}
};

static void PrintContents(int contents)
{
	int i;

	for (i = 0; contentnames[i].value; i++)
	{
		if (contents & contentnames[i].value)
		{
			Log_Write("%s,", contentnames[i].name);
		}
	}
}

static void Q3_BSPBrushToMapBrush(dbrush_t *bspbrush, entity_t *mapent)
{
	mapbrush_t *b;
	int k, n;
	side_t *side, *s2;
	int planenum;
	int ladderclip = 0;
	dbrushside_t *bspbrushside;
	dplane_t *bspplane;

	if (nummapbrushes >= MAX_MAP_BRUSHES)
		Error ("nummapbrushes >= MAX_MAP_BRUSHES");

	b = &mapbrushes[nummapbrushes];
	b->original_sides = &brushsides[nummapbrushsides];
	b->entitynum = mapent-entities;
	b->brushnum = nummapbrushes - mapent->firstbrush;
	b->leafnum = -1;  // not used by anything

	for (n = 0; n < bspbrush->numSides; n++)
	{
		//pointer to the bsp brush side
		bspbrushside = &q3bsp->brushSides[bspbrush->firstSide + n];

		if (nummapbrushsides >= MAX_MAP_BRUSHSIDES)
		{
			Error ("MAX_MAP_BRUSHSIDES");
		}
		//pointer to the map brush side
		side = &brushsides[nummapbrushsides];
		//if the BSP brush side is textured
		if (q3_dbrushsidetextured[bspbrush->firstSide + n])
			side->flags |= SFL_TEXTURED|SFL_VISIBLE;
		else
			side->flags &= ~SFL_TEXTURED;
		if (bspbrushside->shaderNum < 0)
		{
			side->contents = 0;
			side->surf = 0;
		}
		else
		{
			side->contents = q3bsp->shaders[bspbrushside->shaderNum].contentFlags;
			side->surf = q3bsp->shaders[bspbrushside->shaderNum].surfaceFlags;

			if (strstr(q3bsp->shaders[bspbrushside->shaderNum].shader, "common/hint"))
			{
				//Log_Print("found hint side\n");
				side->surf |= SURF_HINT;
			}

			// andrewj: detect "ladderclip" by name, not by surfaceparm
			side->surf &= ~SURF_LADDER;

			if (strstr(q3bsp->shaders[bspbrushside->shaderNum].shader, "common/ladderclip"))
			{
				//Log_Print("found a ladderclip side.\n");
				ladderclip = 1;
			}
		}

		if (side->surf & SURF_NODRAW)
		{
			side->flags |= SFL_TEXTURED|SFL_VISIBLE;
		}
		/*
		if (side->contents & (CONTENTS_TRANSLUCENT|CONTENTS_STRUCTURAL))
		{
			side->flags |= SFL_TEXTURED|SFL_VISIBLE;
		} */

		// hints and skips are never detail, and have no content
		if (side->surf & (SURF_HINT|SURF_SKIP) )
		{
			side->contents = 0;
			//Log_Print("found hint brush side\n");
		}
		/*
		if ((side->surf & SURF_NODRAW) && (side->surf & SURF_NOIMPACT))
		{
			side->contents = 0;
			side->surf &= ~CONTENTS_DETAIL;
			Log_Print("probably found hint brush in a BSP without hints being used\n");
		} */

		//ME: get a plane for this side
		bspplane = &q3bsp->planes[bspbrushside->planeNum];
		planenum = FindFloatPlane(bspplane->normal, bspplane->dist);
		//
		// see if the plane has been used already
		//
		//ME: this really shouldn't happen!!!
		//ME: otherwise the bsp file is corrupted??
		//ME: still it seems to happen, maybe Johny Boy's
		//ME: brush bevel adding is crappy ?
		for (k = 0; k < b->numsides; k++)
		{
			s2 = b->original_sides + k;
//			if (DotProduct (mapplanes[s2->planenum].normal, mapplanes[planenum].normal) > 0.999
//							&& fabs(mapplanes[s2->planenum].dist - mapplanes[planenum].dist) < 0.01 )

			if (s2->planenum == planenum)
			{
				Log_Print("Entity %i, Brush %i: duplicate plane\n"
					, b->entitynum, b->brushnum);
				break;
			}
			if ( s2->planenum == (planenum^1) )
			{
				Log_Print("Entity %i, Brush %i: mirrored plane\n"
					, b->entitynum, b->brushnum);
				break;
			}
		}
		if (k != b->numsides)
			continue;		// duplicated

		//
		// keep this side
		//
		side = b->original_sides + b->numsides;
		side->planenum = planenum;

		nummapbrushsides++;
		b->numsides++;
	}

	// get the content for the entire brush
	b->contents = q3bsp->shaders[bspbrush->shaderNum].contentFlags;
	b->contents &= ~(CONTENTS_FOG|CONTENTS_STRUCTURAL);

	if (ladderclip) b->contents |= CONTENTS_LADDERCLIP;

	if (BrushExists(b))
	{
		c_squattbrushes++;
		b->numsides = 0;
		return;
	}

	//create the AAS brushes from this brush, don't add brush bevels
	AAS_CreateMapBrushes(b, mapent, qfalse);
}

static void Q3_ParseBSPBrushes(entity_t *mapent)
{
	int i;

	for (i = 0; i < q3bsp->submodels[mapent->modelnum].numBrushes; i++)
	{
		Q3_BSPBrushToMapBrush(&q3bsp->brushes[q3bsp->submodels[mapent->modelnum].firstBrush + i], mapent);
	}
}

static qboolean Q3_ParseBSPEntity(int entnum)
{
	entity_t *mapent;
	char *model;

	mapent = &entities[entnum];//num_entities];
	mapent->firstbrush = nummapbrushes;
	mapent->numbrushes = 0;
	mapent->modelnum = -1;	//-1 = no BSP model

	model = ValueForKey(mapent, "model");
	if (model && strlen(model))
	{
		if (*model == '*')
		{
			//get the model number of this entity (skip the leading *)
			mapent->modelnum = atoi(&model[1]);
		}
	}

	GetVectorForKey(mapent, "origin", mapent->origin);

	//if this is the world entity it has model number zero
	//the world entity has no model key
	if (!strcmp("worldspawn", ValueForKey(mapent, "classname")))
	{
		mapent->modelnum = 0;
	}
	//if the map entity has a BSP model (a modelnum of -1 is used for
	//entities that aren't using a BSP model)
	if (mapent->modelnum >= 0)
	{
		//parse the bsp brushes
		Q3_ParseBSPBrushes(mapent);
	}
	//
	//the origin of the entity is already taken into account
	//
	//func_group entities can't be in the bsp file
	//
	//check out the func_areaportal entities
	if (!strcmp ("func_areaportal", ValueForKey (mapent, "classname")))
	{
		c_areaportals++;
		mapent->areaportalnum = c_areaportals;
		return qtrue;
	}
	return qtrue;
}

#define	MAX_PATCH_VERTS		1024

static void AAS_CreateCurveBrushes(void)
{
	int i, j, n, planenum, numcurvebrushes = 0;
	dsurface_t *surface;
	drawVert_t *dv_p;
	vec3_t points[MAX_PATCH_VERTS];
	int width, height, c;
	patchCollide_t *pc;
	facet_t *facet;
	mapbrush_t *brush;
	side_t *side;
	entity_t *mapent;
	winding_t *winding;

	Log_Print("nummapbrushsides = %d\n", nummapbrushsides);
	Progress_Begin("curve brushes", q3bsp->numSurfaces, 0);
	mapent = &entities[0];

	for (i = 0; i < q3bsp->numSurfaces; i++)
	{
		surface = &q3bsp->surfaces[i];
		if ( ! surface->patchWidth ) continue;
		// if the curve is not solid
		if (!(q3bsp->shaders[surface->shaderNum].contentFlags & (CONTENTS_SOLID|CONTENTS_PLAYERCLIP)))
		{
			//Log_Print("skipped non-solid curve\n");
			continue;
		}
		// if this curve should not be used for AAS
		if ( q3bsp->shaders[surface->shaderNum].contentFlags & CONTENTS_NOBOTCLIP ) {
			continue;
		}
		//
		width = surface->patchWidth;
		height = surface->patchHeight;
		c = width * height;
		if (c > MAX_PATCH_VERTS)
		{
			Error("ParseMesh: MAX_PATCH_VERTS");
		}

		dv_p = q3bsp->drawVerts + surface->firstVert;
		for ( j = 0 ; j < c ; j++, dv_p++ )
		{
			points[j][0] = dv_p->xyz[0];
			points[j][1] = dv_p->xyz[1];
			points[j][2] = dv_p->xyz[2];
		}
		// create the internal facet structure
		pc = CM_GeneratePatchCollide(width, height, points, surface->subdivisions);

		for (j = 0; j < pc->numFacets; j++)
		{
			facet = &pc->facets[j];

			brush = &mapbrushes[nummapbrushes];
			brush->original_sides = &brushsides[nummapbrushsides];
			brush->entitynum = 0;
			brush->brushnum = nummapbrushes - mapent->firstbrush;

			brush->numsides = facet->numBorders + 2;
			nummapbrushsides += brush->numsides;
			brush->contents = CONTENTS_SOLID;

			Progress_Update(i);

			planenum = FindFloatPlane(pc->planes[facet->surfacePlane].plane, pc->planes[facet->surfacePlane].plane[3]);

			side = &brush->original_sides[0];
			side->planenum = planenum;
			side->contents = CONTENTS_SOLID;
			side->flags |= SFL_TEXTURED|SFL_VISIBLE|SFL_CURVE;
			side->surf = 0;

			side = &brush->original_sides[1];
			if (1) // create_aas
			{
				//the plane is expanded later so it's not a problem that
				//these first two opposite sides are coplanar
				side->planenum = planenum ^ 1;
			}
			else
			{
				side->planenum = FindFloatPlane(mapplanes[planenum^1].normal, mapplanes[planenum^1].dist + 1);
				side->flags |= SFL_TEXTURED|SFL_VISIBLE;
			}
			side->contents = CONTENTS_SOLID;
			side->flags |= SFL_CURVE;
			side->surf = 0;

			winding = BaseWindingForPlane(mapplanes[side->planenum].normal, mapplanes[side->planenum].dist);
			for (n = 0; n < facet->numBorders; n++)
			{
				//never use the surface plane as a border
				if (facet->borderPlanes[n] == facet->surfacePlane) continue;
				//
				side = &brush->original_sides[2 + n];
				side->planenum = FindFloatPlane(pc->planes[facet->borderPlanes[n]].plane, pc->planes[facet->borderPlanes[n]].plane[3]);
				if (facet->borderInward[n]) side->planenum ^= 1;
				side->contents = CONTENTS_SOLID;
				side->flags |= SFL_TEXTURED|SFL_CURVE;
				side->surf = 0;
				//chop the winding in place
				if (winding) ChopWindingInPlace(&winding, mapplanes[side->planenum^1].normal, mapplanes[side->planenum^1].dist, 0.1); //CLIP_EPSILON);
			}
			//VectorCopy(pc->bounds[0], brush->mins);
			//VectorCopy(pc->bounds[1], brush->maxs);
			if (!winding)
			{
				Warning("AAS_CreateCurveBrushes: no winding\n");
				brush->numsides = 0;
				continue;
			}
			brush->original_sides[0].winding = winding;
			WindingBounds(winding, brush->mins, brush->maxs);
			for (n = 0; n < 3; n++)
			{
				//IDBUG: all the indexes into the mins and maxs were zero (not using i)
				if (brush->mins[n] < -MAX_MAP_BOUNDS || brush->maxs[n] > MAX_MAP_BOUNDS)
				{
					Log_Print("entity %i, brush %i: bounds out of range\n", brush->entitynum, brush->brushnum);
					Log_Print("brush->mins[%d] = %f, brush->maxs[%d] = %f\n", n, brush->mins[n], n, brush->maxs[n]);
					brush->numsides = 0; //remove the brush
					break;
				}
				if (brush->mins[n] > MAX_MAP_BOUNDS || brush->maxs[n] < -MAX_MAP_BOUNDS)
				{
					Log_Print("entity %i, brush %i: no visible sides on brush\n", brush->entitynum, brush->brushnum);
					Log_Print("brush->mins[%d] = %f, brush->maxs[%d] = %f\n", n, brush->mins[n], n, brush->maxs[n]);
					brush->numsides = 0; //remove the brush
					break;
				}
			}
			if (1) // create_aas
			{
				//NOTE: brush bevels already added
				AAS_CreateMapBrushes(brush, mapent, qfalse);
			}
			else
			{
				// create windings for sides and bounds for brush
				MakeBrushWindings(brush);
				AddBrushBevels(brush);
				nummapbrushes++;
				mapent->numbrushes++;
			}
		}
	}
	Progress_End();
	Log_Print("%6d curve brushes\n", numcurvebrushes);
}

void AAS_ExpandMapBrush(mapbrush_t *brush, vec3_t mins, vec3_t maxs);
static void Q3_FindVisibleBrushSides(void);
static void Q3_CountTriangles(void);
static void Q3_ParseEntities(void);

void Q3_LoadMapFromBSP(const char *filename)
{
	int i;
	//vec3_t mins = {-1,-1,-1}, maxs = {1, 1, 1};

	Log_Print("-- Q3_LoadMapFromBSP --\n");

	Log_Print("Loading map from %s...\n", filename);

	q3bsp = BSP_Load(filename);

	if (! q3bsp)
		Error("failed to load bsp file\n");

	Q3_CountTriangles();
	Q3_FindVisibleBrushSides();

	nummapbrushsides = 0;
	num_entities = 0;

	Q3_ParseEntities();

	for (i = 0; i < num_entities; i++)
	{
		Q3_ParseBSPEntity(i);
	}

	AAS_CreateCurveBrushes();

	//get the map mins and maxs from the world model
	ClearBounds(map_mins, map_maxs);
	for (i = 0; i < entities[0].numbrushes; i++)
	{
		if (mapbrushes[i].numsides <= 0)
			continue;
		AddPointToBounds (mapbrushes[i].mins, map_mins, map_maxs);
		AddPointToBounds (mapbrushes[i].maxs, map_mins, map_maxs);
	}
	/*
	for (i = 0; i < nummapbrushes; i++)
	{
		//if (!mapbrushes[i].original_sides) continue;
		//AddBrushBevels(&mapbrushes[i]);
		//AAS_ExpandMapBrush(&mapbrushes[i], mins, maxs);
	} */
	/*
	for (i = 0; i < nummapbrushsides; i++)
	{
		Log_Write("side %d flags = %d", i, brushsides[i].flags);
	}
	for (i = 0; i < nummapbrushes; i++)
	{
		Log_Write("brush contents: ");
		PrintContents(mapbrushes[i].contents);
		Log_Print("\n");
	} */
}

void Q3_ResetMapLoading(void)
{
}


//=============================================================================

bspFile_t		*q3bsp = NULL;

char			q3_dbrushsidetextured[MAX_MAP_BRUSHSIDES];


void Q3_FreeMaxBSP(void)
{
	BSP_Free( q3bsp );
	q3bsp = NULL;
}

static void Q3_SurfacePlane(dsurface_t *surface, vec3_t normal, float *dist)
{
	int i;
	float *p0, *p1, *p2;
	vec3_t t1, t2;

	VectorClear( t1 );
	VectorClear( t2 );

	p0 = q3bsp->drawVerts[surface->firstVert].xyz;
	for (i = 1; i < surface->numVerts-1; i++)
	{
		p1 = q3bsp->drawVerts[surface->firstVert + ((i) % surface->numVerts)].xyz;
		p2 = q3bsp->drawVerts[surface->firstVert + ((i+1) % surface->numVerts)].xyz;
		VectorSubtract(p0, p1, t1);
		VectorSubtract(p2, p1, t2);
		CrossProduct(t1, t2, normal);
		VectorNormalize(normal);
		if (VectorLength(normal)) break;
	}
/*
	float dot;
	for (i = 0; i < surface->numVerts; i++)
	{
		p0 = q3bsp->drawVerts[surface->firstVert + ((i) % surface->numVerts)].xyz;
		p1 = q3bsp->drawVerts[surface->firstVert + ((i+1) % surface->numVerts)].xyz;
		p2 = q3bsp->drawVerts[surface->firstVert + ((i+2) % surface->numVerts)].xyz;
		VectorSubtract(p0, p1, t1);
		VectorSubtract(p2, p1, t2);
		VectorNormalize(t1);
		VectorNormalize(t2);
		dot = DotProduct(t1, t2);
		if (dot > -0.9 && dot < 0.9 &&
			VectorLength(t1) > 0.1 && VectorLength(t2) > 0.1) break;
	}
	CrossProduct(t1, t2, normal);
	VectorNormalize(normal);
*/
	if (VectorLength(normal) < 0.9)
	{
		Log_Print("surface %u bogus normal vector %f %f %f\n", (unsigned int)(surface - q3bsp->surfaces), normal[0], normal[1], normal[2]);
		Log_Print("t1 = %f %f %f, t2 = %f %f %f\n", t1[0], t1[1], t1[2], t2[0], t2[1], t2[2]);
		for (i = 0; i < surface->numVerts; i++)
		{
			p1 = q3bsp->drawVerts[surface->firstVert + ((i) % surface->numVerts)].xyz;
			Log_Print("p%d = %f %f %f\n", i, p1[0], p1[1], p1[2]);
		}
	}
	*dist = DotProduct(p0, normal);
}

dplane_t *q3_surfaceplanes;

static void Q3_CreatePlanarSurfacePlanes(void)
{
	int i;
	dsurface_t *surface;

	Log_Print("creating planar surface planes...\n");
	q3_surfaceplanes = (dplane_t *) GetClearedMemory(q3bsp->numSurfaces * sizeof(dplane_t));

	for (i = 0; i < q3bsp->numSurfaces; i++)
	{
		surface = &q3bsp->surfaces[i];
		if (surface->surfaceType != MST_PLANAR) continue;
		Q3_SurfacePlane(surface, q3_surfaceplanes[i].normal, &q3_surfaceplanes[i].dist);
		//Log_Print("normal = %f %f %f, dist = %f\n", q3_surfaceplanes[i].normal[0],
		//											q3_surfaceplanes[i].normal[1],
		//											q3_surfaceplanes[i].normal[2], q3_surfaceplanes[i].dist);
	}
}

static void Q3_FreePlanarSurfacePlanes(void)
{
	if ( q3_surfaceplanes ) FreeMemory( q3_surfaceplanes );
	q3_surfaceplanes = NULL;
}

/*
static void Q3_SurfacePlane(dsurface_t *surface, vec3_t normal, float *dist)
{
	//take the plane information from the lightmap vector
	// VectorCopy(surface->lightmapVecs[2], normal);
	//calculate plane dist with first surface vertex
	// *dist = DotProduct(q3bsp->drawVerts[surface->firstVert].xyz, normal);
	Q3_PlaneFromPoints(q3bsp->drawVerts[surface->firstVert].xyz,
						q3bsp->drawVerts[surface->firstVert+1].xyz,
						q3bsp->drawVerts[surface->firstVert+2].xyz, normal, dist);
} */

// returns the amount the face and the winding overlap
static float Q3_FaceOnWinding(dsurface_t *surface, winding_t *winding)
{
	int i;
	float dist, area;
	dplane_t plane;
	vec_t *v1, *v2;
	vec3_t normal, edgevec;
	winding_t *w;

	//copy the winding before chopping
	w = CopyWinding(winding);
	//retrieve the surface plane
	Q3_SurfacePlane(surface, plane.normal, &plane.dist);
	//chop the winding with the surface edge planes
	for (i = 0; i < surface->numVerts && w; i++)
	{
		v1 = q3bsp->drawVerts[surface->firstVert + ((i) % surface->numVerts)].xyz;
		v2 = q3bsp->drawVerts[surface->firstVert + ((i+1) % surface->numVerts)].xyz;
		//create a plane through the edge from v1 to v2, orthogonal to the
		//surface plane and with the normal vector pointing inward
		VectorSubtract(v2, v1, edgevec);
		CrossProduct(edgevec, plane.normal, normal);
		VectorNormalize(normal);
		dist = DotProduct(normal, v1);
		//
		ChopWindingInPlace(&w, normal, dist, -0.1); //CLIP_EPSILON
	}
	if (w)
	{
		area = WindingArea(w);
		FreeWinding(w);
		return area;
	}
	return 0;
}

// creates a winding for the given brush side on the given brush
static winding_t *Q3_BrushSideWinding(dbrush_t *brush, dbrushside_t *baseside)
{
	int i;
	dplane_t *baseplane, *plane;
	winding_t *w;
	dbrushside_t *side;

	//create a winding for the brush side with the given planenumber
	baseplane = &q3bsp->planes[baseside->planeNum];
	w = BaseWindingForPlane(baseplane->normal, baseplane->dist);
	for (i = 0; i < brush->numSides && w; i++)
	{
		side = &q3bsp->brushSides[brush->firstSide + i];
		//don't chop with the base plane
		if (side->planeNum == baseside->planeNum) continue;
		//also don't use planes that are almost equal
		plane = &q3bsp->planes[side->planeNum];
		if (DotProduct(baseplane->normal, plane->normal) > 0.999
				&& fabs(baseplane->dist - plane->dist) < 0.01) continue;
		//
		plane = &q3bsp->planes[side->planeNum^1];
		ChopWindingInPlace(&w, plane->normal, plane->dist, -0.1); //CLIP_EPSILON);
	}
	return w;
}

qboolean WindingIsTiny(winding_t *w);

// fix screwed brush texture references
static void Q3_FindVisibleBrushSides(void)
{
	int i, j, k, we, numtextured, numsides;
	float dot;
	dplane_t *plane;
	dbrushside_t *brushside;
	dbrush_t *brush;
	dsurface_t *surface;
	winding_t *w;

	memset(q3_dbrushsidetextured, qfalse, MAX_MAP_BRUSHSIDES);
	//
	numsides = 0;
	//go over all the brushes
	if ( forcesidesvisible ) {
		Log_Print("searching nodraw brush sides...\n");
		for ( i = 0; i < q3bsp->numBrushSides; i++ ) {
			if ( q3bsp->shaders[q3bsp->brushSides[i].shaderNum].surfaceFlags & SURF_NODRAW ) {
				q3_dbrushsidetextured[i] = qfalse;
			}
			else
			{
				q3_dbrushsidetextured[i] = qtrue;
			}
		}
	} else {
		//create planes for the planar surfaces
		Q3_CreatePlanarSurfacePlanes();
		Log_Print("searching visible brush sides...\n");
		Progress_Begin("brush sides", q3bsp->numBrushes, 0);

		for (i = 0; i < q3bsp->numBrushes; i++)
		{
			brush = &q3bsp->brushes[i];
			//go over all the sides of the brush
			for (j = 0; j < brush->numSides; j++)
			{
				numsides++;
				brushside = &q3bsp->brushSides[brush->firstSide + j];
				//
				if ( q3bsp->shaders[brushside->shaderNum].surfaceFlags & SURF_NODRAW ) {
					continue;
				}
				//
				w = Q3_BrushSideWinding(brush, brushside);
				if (!w)
				{
					q3_dbrushsidetextured[brush->firstSide + j] = qtrue;
					continue;
				}
				else
				{
					//RemoveEqualPoints(w, 0.2);
					if (WindingIsTiny(w))
					{
						FreeWinding(w);
						q3_dbrushsidetextured[brush->firstSide + j] = qtrue;
						continue;
					}
					else
					{
						we = WindingError(w);
						if (we == WE_NOTENOUGHPOINTS
							|| we == WE_SMALLAREA
							|| we == WE_POINTBOGUSRANGE
	//						|| we == WE_NONCONVEX
							)
						{
							FreeWinding(w);
							q3_dbrushsidetextured[brush->firstSide + j] = qtrue;
							continue;
						}
					}
				}
				if (WindingArea(w) < 20)
				{
					q3_dbrushsidetextured[brush->firstSide + j] = qtrue;
					continue;
				}
				//find a face for texturing this brush
				for (k = 0; k < q3bsp->numSurfaces; k++)
				{
					surface = &q3bsp->surfaces[k];
					if (surface->surfaceType != MST_PLANAR) continue;
					//
					//Q3_SurfacePlane(surface, plane.normal, &plane.dist);
					plane = &q3_surfaceplanes[k];
					//the surface plane and the brush side plane should be pretty much the same
					if (fabs(fabs(plane->dist) - fabs(q3bsp->planes[brushside->planeNum].dist)) > 5) continue;
					dot = DotProduct(plane->normal, q3bsp->planes[brushside->planeNum].normal);
					if (dot > -0.9 && dot < 0.9) continue;
					//if the face is partly or totally on the brush side
					if (Q3_FaceOnWinding(surface, w))
					{
						q3_dbrushsidetextured[brush->firstSide + j] = qtrue;
						//Log_Write("Q3_FaceOnWinding");
						break;
					}
				}
				FreeWinding(w);
			}
			Progress_Update(i);
		}
		Progress_End();
		Log_Print("%6d brush sides\n", numsides);
		Q3_FreePlanarSurfacePlanes();
	}
	numtextured = 0;
	for (i = 0; i < q3bsp->numBrushSides; i++)
	{
		if (q3_dbrushsidetextured[i]) numtextured++;
	}
	Log_Print("%d brush sides textured out of %d\n", numtextured, q3bsp->numBrushSides);
}

static void Q3_CountTriangles(void)
{
	int i, numTris, numPatchTris;
	dsurface_t *surface;

	numTris = numPatchTris = 0;
	for ( i = 0; i < q3bsp->numSurfaces; i++ ) {
		surface = &q3bsp->surfaces[i];

		numTris += surface->numIndexes / 3;

		if ( surface->patchWidth ) {
			numPatchTris += surface->patchWidth * surface->patchHeight * 2;
		}
	}

	Log_Print( "%6d triangles\n", numTris );
	Log_Print( "%6d patch tris\n", numPatchTris );
}


//============================================================================

/*
================
Q3_ParseEntities

Parses the entity string into entities
================
*/
static void Q3_ParseEntities (void)
{
	script_t *script;

	num_entities = 0;
	script = LoadScriptMemory(q3bsp->entityString, q3bsp->entityStringLength, "*Quake3 bsp file");
	SetScriptFlags(script, SCFL_NOSTRINGWHITESPACES |
									SCFL_NOSTRINGESCAPECHARS);

	while(ParseEntity(script))
	{
	}

	FreeScript(script);
}

